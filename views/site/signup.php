<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

?>
<div class="site-login">
    <div class="col-xs-12" style="margin-top: 20%">
        <div style="text-align: center;color:#4a4e51;"><img src="/images/logo_green.png" style="width:50%;"></div>
        <h3 style="text-align: center;color:#4a4e51;">Войти</h3>
        <p style="text-align: center;color:#4a4e51;">Базелик — это сервис для специалистов. И другое описание этого проекта.</p>
    
        <?php $form = ActiveForm::begin(['id' => 'form-signup',
            'options' => ['class' => 'form-horizontal'],
            'fieldConfig' => [
                    'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-7\">{error}</div>",
                    'labelOptions' => ['class' => 'col-lg-2 control-label'],
            ],

                ]); ?>
            <?= $form->field($newModel, 'username')->textInput(['placeholder' => 'Логин'])->label(false); ?>
            <?= $form->field($newModel, 'email')->textInput(['placeholder' => 'E-mail'])->label(false); ?>
            <?= $form->field($newModel, 'password')->passwordInput(['placeholder' => 'Пароль'])->label(false); ?>

            <div class="form-group">
                <div class="col-lg-offset-1 col-lg-11">
                    <?= Html::submitButton('Зарегистрироваться', ['class' => 'btn btn-primary  signup-button']) ?>
                    <?= Html::a(Yii::t('main', 'Авторизоваться'), '/site/login', ['class' => 'btn btn-primary login-button']) ?>
                </div>
            </div>
        <?php ActiveForm::end(); ?>
            
        <div style="margin-top: 40px;text-align: center">
            <p style="color:#484f54;text-align: center;margin-bottom: 0px">Регистрируясь, вы принимаете условия</p>
            <a style="color:64aa48;text-align: center">пользовательского соглашения</a>
        </div>
            
    </div>
</div>
