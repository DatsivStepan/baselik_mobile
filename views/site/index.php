<?php

/* @var $this yii\web\View */

$this->title = 'My Yii Application';

?>


<div id="container-main" class="container-main" style="position: relative;">

    <nav class="st-menu st-effect-1 hidden-sm hidden-md hidden-lg" id="menu-1"
         style="background-color: white;position:fixed;">
        <?= $this->render('left_menu'); ?>
    </nav>
    <div class="st-pusher"></div>
    <div class="" style="min-height: 100vh">
        <div class="col-sm-3 hidden-xs"
             style="min-height: 100vh;background-color: white;position: relative;padding:0px;">

            <div class="" style="position:fixed;width: inherit;background-color: white;">
                <?= $this->render('left_menu'); ?>
            </div>

        </div>
        <div class="col-sm-9 co-xs-12" style="min-height: 100vh;padding: 0px;position: relative">


            <!-- // -->
            <!-- Favorite begin -->
            <!-- // -->
            <div style='position:relative;'>
                <div class="favorite_block"
                     style="position:fixed;width:inherit;height:100vh;z-index:100;background-color: white;top:0;display:none;    overflow: auto;">
                    <div style="background-color: #80c565;padding: 13px 8px 12px 10px;">
                        <div class="favotite_arrow_back" style="display:inline-block;">
                        </div>
                        <div style="display:inline-block;color: white;font-size: 24px;padding-left: 10px;">Избранное
                        </div>
                    </div>
                    <div style="position: relative">
                        <div class="col-xs-12"
                             style="margin:0px;background-color: white;position:relative;padding-bottom: 46px">
                            <div style="padding:20px 0px 20px 0px" class=" favoriteVideoBlock clf">
                            </div>
                            <div style="clear:both;"></div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- // -->
            <!-- Favorite end -->
            <!-- // -->


            <div style='position:relative;'>
                <div class="video_show"
                     style="position:fixed;width:inherit;height:100vh;width:inherit;z-index:100;background-color: white;top:0;display:none;"></div>
            </div>

            <!-- // -->
            <!-- Profile begin -->
            <!-- // -->
            <div style='position:relative;'>
                <div class="profile_show"
                     style="overflow: auto;position:fixed;height:100vh;width:inherit;z-index:100;background-color: white;top:0;display:none;">
                    <div class="heig_cl_23" style="background-color: #80c565;padding: 27px 8px 12px 10px;height: 90px;margin-bottom: 25px;">
                        <div class="profile_show_arrow_back" style="display:inline-block;">
                        </div>
                        <div style="display:inline-block;color: white;font-size: 24px;padding-left: 10px;">Профиль</div>
                    </div>
                    <div style="position: relative">
                        <div class="col-xs-12 col-sm-5">
                            <div style="padding:15px 15px 0px 5px;text-align: left;">
                                <button class="edit_profile_button btn">
                                    <img src="/images/edit_icon.png" style="width:16px;height:16px;">
                                    Редактировать
                                </button>
                                <button class="cancel_profile_button btn" style="display:none;">
                                    <span class="fa fa-times"></span>
                                    Отменить
                                </button>
                                <div style="clear: both"></div>
                            </div>
                            <div style="text-align: center;padding-bottom: 15px">
                                <img class="user_avatar" src="http://baselik.roketdev.pro/img/default_avatar.png"
                                     style="width:40%;border-radius: 50%">
                                <p style="text-align: center;color:#788277;font-size:22px;margin-top: 15px"
                                   class="p_roman_dd username_b"></p>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-7 user_information_block" style="margin-top: 10px">
                            <p><b>Пол</b> &nbsp;&nbsp;&nbsp;<span class="gender"></span></p>
                            <p><b>Дата Рождения</b> &nbsp;&nbsp;&nbsp;<span class="birthday"></span></p>
                            <p><b>На сайте</b> &nbsp;&nbsp;&nbsp;<span class="in_site"></span></p>
                            <p><b>Кол-во специализаций &nbsp;&nbsp;&nbsp;</b> <span class="spec_count"></span></p>
                            <p><b>Логин в системе</b> &nbsp;&nbsp;&nbsp;<span class="username"></span></p>
                            <p><b>Email</b> &nbsp;&nbsp;&nbsp;<span class="email"></span></p>
                        </div>
                        <div class="col-xs-12 col-sm-7 user_change_information_block"
                             style="display:none;color:#7b8185;">
                            <div style="text-align: center;padding-bottom: 15px">
                                <img class="user_avatar_change" src="http://baselik.roketdev.pro/img/default_avatar.png"
                                     style="width:40%;border-radius: 50%">
                                <input type="hidden" id="avatar_src">
                                <div>
                                    <button class="btn change_avatar_button"
                                            style="background-color: #80c565;margin-top: 5px;padding-left: 20px;padding-right: 20px;border-radius: 29px;padding-bottom: 8px;padding-top: 8px;display:none;">
                                        <img src="/images/upload_icon.png" style="width:16px;height:16px;">
                                        Изменить аватар
                                    </button>
                                </div>
                            </div>
                            <p class="p_name_sett_dd" style="margin-bottom: 0px;margin-top: 5px">Имя</p>
                            <input type="text" name="name" class="form-control input_css_red">

                            <p class="p_name_sett_dd" style="margin-bottom: 0px;margin-top: 5px">Фамилия</p>
                            <input type="text" name="surname" class="form-control input_css_red">

                            <p class="p_name_sett_dd" style="margin-bottom: 0px;margin-top: 5px">Пол</p>
                            <label class="radio-inline">
                                <input type="radio" id="man" name="gender" value="0" class="p_name_sett_dd">Мужской
                            </label>
                            <label class="radio-inline">
                                <input type="radio" id="woman" name="gender" value="1" class="p_name_sett_dd">Женский
                            </label>

                            <p class="p_name_sett_dd" style="margin-bottom: 0px;margin-top: 14px">Дата Рождения</p>
                            <input type="date" name="birthday" class="form-control input_css_red">

                            <p class="p_name_sett_dd" style="margin-bottom: 0px;margin-top: 5px">Email</p>
                            <input type="email" name="email" class="form-control input_css_red">

                            <button class="btn pull-right saveProfileData p_name_sett_dd"
                                    style="margin-top: 10px;background-color: #80c565;margin-top: 5px;padding-left: 20px;padding-right: 20px;border-radius: 29px;padding-bottom: 8px;padding-top: 8px;">
                                <img src="/images/save_b_icon.png" style="width:16px;height:16px;">
                                Сохранить
                            </button>
                        </div>
                    </div>
                </div>
            </div>

            <!-- // -->
            <!-- Profile end -->
            <!-- // -->


            <!-- // -->
            <!-- contact begin -->
            <!-- // -->
            <div style='position:relative;'>
                <div class="contact_block"
                     style="position:fixed;width:100%;height:100vh;z-index:100;background-color: white;top:0;display:none;">
                    <div style="background-color: #80c565;padding: 13px 8px 12px 10px;">
                        <div class="contact_arrow_back" style="display:inline-block;">
                        </div>
                        <div style="display:inline-block;color: white;font-size: 24px;padding-left: 10px;">Контакты
                        </div>
                    </div>
                    <div style="position: relative">
                        <div class="col-sm-3 hidden-xs" style="padding:0px;">
                        </div>
                        <div class="col-sm-10 col-xs-12" style="padding-top: 10px">
                            <div class="col-xs-12 col-sm-12 user_information_block">
                                <button class="edit_profile_button btn" style="float:left;">
                                    <img src="/images/edit_icon.png" style="width:16px;height:16px;">
                                    Редактировать
                                </button>
                                <div style='clear:both;'></div>
                                <div style='margin-top:10px;'></div>
                                <p><b>Местонахождение(город):</b> &nbsp;&nbsp;&nbsp;<span class="address"></span></p>
                                <p><b>Контактный телефон</b> &nbsp;&nbsp;&nbsp;<span class="telefon"></span></p>
                                <p><b>E-mail</b> &nbsp;&nbsp;&nbsp;<span class="email"></span></p>
                                <p><b>Время работы: </b> &nbsp;&nbsp;&nbsp;<span class="work_time"></span></p>
                                <p><b>Whatsapp:</b> &nbsp;&nbsp;&nbsp;<span class="whatsapp"></span></p>
                                <p><b>Viber:</b> &nbsp;&nbsp;&nbsp;<span class="viber"></span></p>
                                <p><b>Skype:</b> &nbsp;&nbsp;&nbsp;<span class="email"></span></p>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- // -->
                <!-- contact end -->
                <!-- // -->


                <!-- // -->
                <!-- Filter begin -->
                <!-- // -->
                <div style='position:relative;'>
                    <div class="video_filter"
                         style="position:fixed;width:inherit;height:100vh;z-index:100;background-color: white;top:0;display:none;">
                        <div class="heig_cl_23" style="background-color: #80c565;padding: 27px 8px 12px 10px;height: 90px;margin-bottom: 25px;">
                            <div class="video_filter_arrow_back" style="display:inline-block;">
                            </div>
                            <div style="display:inline-block;color: white;font-size: 26px;padding-left: 10px;">Фильтры
                            </div>
                        </div>
                        <div style="position: relative">
                            <div class="col-sm-3 hidden-xs" style="padding:0px;">
                            </div>
                            <div class="col-sm-11 col-xs-12">
                                <div class="pad_dd">
                                    <input type="text" class="form-control search_filter"
                                           placeholder="Поиск по деятельности или услуге">
                                    <p class="filt_name_23">Категория</p>
                                    <select class="form-control category_filter sel_filt_bas">
                                        <option>Английский язык</option>
                                    </select>
                                    <p class="filt_name_23">Подкатегория</p>
                                    <select class="form-control sub_category_filter sel_filt_bas">
                                        <option>Выбрать</option>
                                    </select>
                                    <div style="margin-top: 20px">
                                        <div class="col-xs-6" style="padding-left: 0px">
                                            <a class="a_sbros">Сбросить</a>
                                        </div>
                                        <div class="col-xs-6" style="padding-right: 0px">
                                            <a class="a_usk">Искать</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- // -->
                <!-- Filter end -->
                <!-- // -->


                <div class="st-pusher" style="position:relative;">

                    <div class="site_header_block" style="position:fixed;z-index:10;">
                        <div class="padd_top_nonr" style="margin-bottom: 10px;padding-top: 35px;">
                            <div id="st-trigger-effects" class="menu_block hidden-sm hidden-md hidden-lg">
                                <button data-effect="st-effect-1"></button>
                            </div>
                            <img src="/images/logo.png" class="site_logo">
                        </div>
                        <div class="input_with_filter_bottom">
                            <div class="input-group">
                                <input type="text" class="form-control">
                                <span class="input-group-btn">
                                  <button class="btn btn-secondary videoFilterButton" type="button"></button>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="row row_padd_top_22"
                         style="margin:0px;background-color: white;position:relative;padding-top:152px;padding-bottom: 46px">
                        <div style="padding:18px 0px 20px 0px" class="video_box videoBoxContent clf">
                        </div>
                        <div style="clear:both;"></div>
                        <div class="col-sm-12" style="text-align: center">
                            <button class="show_more_video_btn" style="margin:0 auto;">
                                <img src="/images/show_more_icon.png" style="width:16px;height:16px;position: relative;bottom: 3px;">
                                Загрузить еще
                            </button>
                        </div>
                    </div>


                    <div style="position:relative;" class="mobile_catalag_footer">
                        <div style="position:fixed;bottom:0px;width: 100%;text-align: center;background:white;">
                            <div style="display:inline-block;padding:10px;margin-bottom: 10px;">
                                <a href="/">
                                    <img class="img_outl_non" src="/images/footer/home_icon.png" style="width: 30px;height:26px;">
                                </a>
                            </div>
                            <div style="display:inline-block;padding:10px;">
                                <img class="img_outl_non" src="/images/footer/star_icon.png" class="favoriteShowButton"
                                     style="width: 30px;height:26px;">
                            </div>
                            <div style="display:inline-block;padding:10px;">
                                <img class="img_outl_non"src="/images/footer/avatar_icon.png" class="home_menu_user"
                                     data-user_id="<?= \Yii::$app->user->id; ?>" style="width: 30px;height:26px;">
                            </div>
                        </div>
                    </div>


                </div>
            </div>

        </div>


    </div>


    <div style="display:none;">
        <div class="row-table-style">
            <div class="table table-striped" class="files" id="previews">
                <div id="template" class="file-row">

                </div>
            </div>
        </div>
    </div>
        
