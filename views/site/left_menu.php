<?php

/* @var $this yii\web\View */
use yii\bootstrap\Html;
$this->title = 'My Yii Application';

?>
    <div class="div_sd_sd" style="padding:30px 18px 5px 5px;text-align: right">
        <img src="/images/account_setting_icon.png" style="width:25px;height:25px;">
    </div>
    <div style="text-align: center;">
        <?php
            $avatar = 'http://baselik.roketdev.pro/img/default_avatar.png';
            if(\Yii::$app->user->identity->avatar){
                $avatar = \Yii::$app->user->identity->avatar;
            }
        ?>
        <img src="<?= $avatar; ?>" class="left_menu_avatar" style="width:120px;height:120px;border-radius: 50%">
        
        <p class="user_name_c"><?= Yii::$app->user->identity->surname.' '.Yii::$app->user->identity->name; ?></p>
    <hr>
    </div>
    <ul class="left_menu_sidebar">
        <li class="one profile_show_button" data-user_id="<?= \Yii::$app->user->id; ?>"><a class="icon icon-data" href="#">Мои данные</a></li>
        <li class="two"><a class="icon icon-location" href="#">Специализация</a></li>
        <li class="three contactShowButton" data-user_id="<?= \Yii::$app->user->id; ?>"><a class="icon icon-study" href="#">Контакты</a></li>
        <li class="four"><a class="icon icon-photo" href="#">Видео</a></li>
        <li class="five favoriteShowButton" ><a class="icon icon-wallet" href="#">Избранное</a></li>
        <li class="six">
            <?= Html::beginForm(['/site/logout'], 'post'); ?>
            <?= Html::submitButton(
                                'Выход',
                                ['class' => 'icon icon-wallet logout']
                            ) ?>
            <?= Html::endForm(); ?>
<!--                <a class="icon icon-wallet" href="#"></a>--> 
            </li>
    </ul>

 