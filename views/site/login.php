<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;

?>
<?php
    if (Yii::$app->getSession()->hasFlash('error')) {
        echo '<div class="alert alert-danger">'.Yii::$app->getSession()->getFlash('error').'</div>';
    }
?>
<div class="site-login">
    <div class='col-sm-3 hidden-xs'></div>
    
    <div class="col-xs-12 col-sm-6" style="margin-top: 20%">
        <div style="text-align: center;color:#4a4e51;"><img src="/images/logo_green.png" style="width:50%;"></div>
        <h3 class="h3_voit">Войти</h3>
        <p class="p_basel_p">Базелик — это сервис для специалистов. И другое описание этого проекта.</p>
        <?php $form = ActiveForm::begin([
            'id' => 'login-form',
            'layout' => 'horizontal',
            'fieldConfig' => [
                'template' => "{label}\n<div class=\"col-lg-12\">{input}</div>\n<div class=\"col-lg-12\">{error}</div>",
                'labelOptions' => ['class' => 'col-lg-12 control-label'],
            ],
        ]); ?>

            <?= $form->field($model, 'username')->textInput(['placeholder' => 'Логин'])->label(false); ?>

            <?= $form->field($model, 'password')->passwordInput(['placeholder' => 'Пароль'])->label(false);; ?>

        <div style="display:none;">
            <?= $form->field($model, 'rememberMe')->checkbox([
                'template' => "<div class=\"col-lg-offset-1 col-lg-3\">{input} {label}</div>\n<div class=\"col-lg-8\">{error}</div>",
            ]) ?>
        </div>
            <?php //= HTML::a(Yii::t('main', 'Forgot password?'),Url::home().'site/request-password-reset');  ?>

            <div class="form-group">
                <div class="col-lg-offset-1 col-lg-11">
                    <?= Html::submitButton('Войти', ['class' => 'btn btn-primary login-button', 'name' => 'login-button']) ?>
                    <?= Html::a(Yii::t('main', 'Регистрация'), Url::home().'site/signup', ['class' => 'btn btn-primary signup-button']) ?>
                </div>
            </div>

        <?php ActiveForm::end(); ?>
        <div style="clear: both"></div>
        <?php echo \nodge\eauth\Widget::widget(['action' => 'site/login']); ?>
        <div style='clear:both;'>
        </div >
        <div class="font_23" style="margin-top: 20%;text-align: center">
            <p style="color:#484f54;text-align: center;margin-bottom: 0px">Авторизуясь, вы принимаете условия</p>
            <a style="color:64aa48;text-align: center">пользовательского соглашения</a>
        </div>
    </div>
</div>
