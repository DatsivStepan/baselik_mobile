<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\widgets\LangWidget;

use app\assets\AdminappAsset;
AdminappAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body class="bg-black skin-black">
<?php $this->beginBody() ?>

        <?php 
            NavBar::begin(['options' => [
                        'style' => 'display:none;',
                    ]]);
            NavBar::end(); ?>
        
     <?php if(!\Yii::$app->user->isGuest){ ?>
    <header class="header">
            <a href="index.html" class="logo">
                <!-- Add the class icon to your logo image or logo icon to add the margining -->
                Yii2 start
            </a>
            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top" role="navigation">
                <!-- Sidebar toggle button-->
                <a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>
                <div class="navbar-right">
                    <ul class="nav navbar-nav">
                        <li class="dropdown user user-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <?=  Html::beginForm(['/site/logout'], 'post'); ?>
                                    <i class="glyphicon glyphicon-user"></i>
                                <?=  Html::submitButton('<span>Logout (' . Yii::$app->user->identity->username . ')</span>',
                                    ['class' => 'btn-link']
                                ); ?>
                                <?= Html::endForm(); ?>
                            </a>
                        </li>
                    </ul>
                </div>
            </nav>
    </header>
     <?php } ?>
    
    
    
    <div class="wrapper row-offcanvas row-offcanvas-left">
            <!-- Left side column. contains the logo and sidebar -->
            <?php if(!\Yii::$app->user->isGuest){ ?>
            <aside class="left-side sidebar-offcanvas">
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    <!-- Sidebar user panel -->
                    <div class="user-panel">
                        <div class="pull-left image">
                            <img src="/images/default_avatar.jpg" class="img-circle" alt="User Image" />
                        </div>
                        <div class="pull-left info">
                            <?php if(!\Yii::$app->user->isGuest){ ?>
                                <p>Hello, <?= \Yii::$app->user->identity->username; ?></p>
                            <?php } ?>

                            <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                        </div>
                    </div>
                    <!-- search form -->
                    <form action="#" method="get" class="sidebar-form">
                        <div class="input-group">
                            <input type="text" name="q" class="form-control" placeholder="Search..."/>
                            <span class="input-group-btn">
                                <button type='submit' name='seach' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
                            </span>
                        </div>
                    </form>
                    <!-- /.search form -->
                    <!-- sidebar menu: : style can be found in sidebar.less -->
                    <ul class="sidebar-menu">
                        <?php
                            $class = ($this->context->getRoute() == 'administration/default/setting')?'active':'';
                            $class2 = ($this->context->getRoute() == 'administration/default/pages')?'active':'';
                        ?>

                            <li class="<?= $class; ?>">
                                <?php echo HTML::a(\Yii::t('app', '<i class="fa fa-cogs"></i>Settings'), Url::home().'administration/default/setting'); ?>
                            </li>
                            <li class="<?= $class2; ?>">
                                <?php echo HTML::a(\Yii::t('app', '<i class="fa fa-file-text"></i>Pages'), Url::home().'administration/default/pages'); ?>
                            </li>
                    </ul>
                </section>
                <!-- /.sidebar -->
            </aside>
            <aside class="right-side">
                <?= Breadcrumbs::widget([
                    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                ]) ?>
                <?= $content ?>
            </aside>
            <?php }else{ ?>
                <?= Breadcrumbs::widget([
                    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                ]) ?>
                <?= $content ?>                
            <?php } ?>
            
            <!-- Right side column. Contains the navbar and content of the page -->
        </div>
    


<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>