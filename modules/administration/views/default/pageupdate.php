<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Alert;
use yii\widgets\LinkPager;
use dosamigos\tinymce\TinyMce;

?>
<section class="content-header">
    <h1 style="color:black;">
        Page update
        <small>Preview</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Page update</li>
    </ol>
</section>

<section class="content">
    <div class="row">
        <!-- left column -->
        <div class="col-md-6" style="margin:0 auto;float:none;">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Quick Example</h3>
                </div><!-- /.box-header -->
                
                <?php $form = ActiveForm::begin(); ?>
                    <div class="box-body" style="color:black;">
                        <?= $form->field($modelPage, 'name')->textinput(); ?>
                        <?= $form->field($modelPage, 'content')->widget(TinyMce::className(), [
                                'options' => ['rows' => 6],
                                'language' => 'en_GB',
                                'clientOptions' => [
                                    'plugins' => [
    //                                                    "advlist autolink lists link charmap print preview anchor",
    //                                                    "searchreplace visualblocks code fullscreen",
                                        "code",
    //                                                    "insertdatetime media table contextmenu paste"
                                    ],
                                    'toolbar' => "undo redo | code | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
                                ]
                            ]);?>
                        <?= $form->field($modelPage, 'sort')->textinput(); ?>
                    </div>
                <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-primary']) ?>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</section>
                                