<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Alert;
use yii\widgets\LinkPager;
use dosamigos\tinymce\TinyMce;
use yii\grid\GridView;
?>
<?php
        if(Yii::$app->session->hasFlash('page_added')):
                echo Alert::widget([
                        'options' => [
                                'class' => 'alert-info',
                        ],
                        'body' => 'Page added',
                ]);
        endif;
        if(Yii::$app->session->hasFlash('page_not_added')):
                echo Alert::widget([
                        'options' => [
                                'class' => 'alert-info',
                        ],
                        'body' => 'Page not added!',
                ]);
        endif;   
        
        if(Yii::$app->session->hasFlash('page_updated')):
                echo Alert::widget([
                        'options' => [
                                'class' => 'alert-info',
                        ],
                        'body' => 'Page updated',
                ]);
        endif;
        if(Yii::$app->session->hasFlash('page_not_updated')):
                echo Alert::widget([
                        'options' => [
                                'class' => 'alert-info',
                        ],
                        'body' => 'Page not updated!',
                ]);
        endif;         
        
        if(Yii::$app->session->hasFlash('page_deleted')):
                echo Alert::widget([
                        'options' => [
                                'class' => 'alert-info',
                        ],
                        'body' => 'Page deleted!',
                ]);
        endif; 
        if(Yii::$app->session->hasFlash('page_not_deleted')):
                echo Alert::widget([
                        'options' => [
                                'class' => 'alert-error',
                        ],
                        'body' => 'Page not deleted!',
                ]);
        endif; 
    ?>


<section class="content-header">
    <h1 style="color:black;">
        Pages list
        <small>Preview</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Pages list</li>
    </ol>
</section>

<section class="content">
    <div class="row">
        <!-- left column -->
        <div class="col-md-6" style="margin:0 auto;float:none;">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Quick Example</h3>
                </div><!-- /.box-header -->

                <!-- form start -->
                    <?php $form = ActiveForm::begin(); ?>
                    <div class="box-body" style="color:black;">
                        <?= $form->field($modelNewPages, 'name')->textinput(); ?>
                        <?= $form->field($modelNewPages, 'content')->widget(TinyMce::className(), [
                            'options' => ['rows' => 6],
                            'language' => 'en_GB',
                            'clientOptions' => [
                                'plugins' => [
//                                                    "advlist autolink lists link charmap print preview anchor",
//                                                    "searchreplace visualblocks code fullscreen",
                                    "code",
//                                                    "insertdatetime media table contextmenu paste"
                                ],
                                'toolbar' => "undo redo | code | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
                            ]
                        ]);?>
                        <?= $form->field($modelNewPages, 'sort')->textinput(); ?>

                        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-primary']) ?>
                    </div>
                    <?php ActiveForm::end(); ?>                                    
                <!-- form end -->

            </div>
        </div>
    </div>
</section>

<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header" style="color:black;">
                    <h3 class="box-title">Responsive Hover Table</h3>
                    <div class="box-tools">
                        <div class="input-group">
                            <input type="text" name="table_search" class="form-control input-sm pull-right" style="width: 150px;" placeholder="Search"/>
                            <div class="input-group-btn">
                                <button class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
                            </div>
                        </div>
                    </div>
                </div><!-- /.box-header -->
                <div class="box-body table-responsive no-padding" style="color:black;">
                        <?= GridView::widget([
                            'dataProvider' => $modelPages,
                            'tableOptions' => [
                                'class' => 'table table-hover'
                            ],
                            'columns' => [
                                'name',
                                'content',
                                [
                                     'class' => 'yii\grid\ActionColumn',
                                     'template' => '{delete} {update}',
                                     'buttons' => [
                                         'delete' => function ($url,$modelPages) {
                                                 return Html::a(
                                                 '<span class="glyphicon glyphicon-trash"></span>', 
                                                 'pagedelete?id='.$modelPages['id']);
                                         },
                                         'update' => function ($url,$modelPages) {
                                                 return Html::a(
                                                 '<span class="glyphicon glyphicon-pencil"></span>', 
                                                 'pageupdate?id='.$modelPages['id']);
                                         },
                                     ],
                                 ],
                            ],
                        ]) ?>
                </div>
            </div>
        </div>
    </div>
</section>