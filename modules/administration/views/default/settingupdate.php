<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Alert;
use yii\widgets\LinkPager;
use yii\grid\GridView;

?>
<section class="content-header">
    <h1 style="color:black;">
        Page update
        <small>Preview</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Page update</li>
    </ol>
</section>

<section class="content">
    <div class="row">
        <!-- left column -->
        <div class="col-md-6" style="margin:0 auto;float:none;">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Quick Example</h3>
                </div><!-- /.box-header -->
                
               <?php $form = ActiveForm::begin(); ?>
                    <div class="box-body" style="color:black;">
                        <?= $form->field($modelSetting, 'key')->textinput(); ?>
                        <?= $form->field($modelSetting, 'name')->textinput(); ?>
                        <?= $form->field($modelSetting, 'value')->textinput(); ?>

                        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-primary']) ?>
                    </div>
                <?php ActiveForm::end(); ?>   
            </div>
        </div>
    </div>
</section>
