<?php

use yii\db\Schema;
use yii\db\Migration;

class m170220_113138_land extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%lang}}', [
            'id' => Schema::TYPE_PK,
            'url' => Schema::TYPE_STRING . '(255) NULL',
            'local' => Schema::TYPE_STRING . '(255) NULL',
            'name' => Schema::TYPE_STRING . '(255) NULL',
            'icon' => Schema::TYPE_STRING . '(255) NULL',
            'default' => Schema::TYPE_SMALLINT . ' NULL DEFAULT 0',
            'date_update' => Schema::TYPE_INTEGER . ' NULL',
            'date_create' => Schema::TYPE_INTEGER . ' NULL',
        ], $tableOptions);

        $this->batchInsert('{{%lang}}', ['url', 'local', 'name', 'icon',  'default', 'date_update', 'date_create'], [
            ['en', 'en-EN', 'English', 'gb.png', 0, time(), time()],
            ['ru', 'ru-RU', 'Русский', 'ru.png', 1, time(), time()],
        ]);
    }

    public function safeDown()
    {
        $this->dropTable('{{%lang}}');
    }
}
