<?php

use yii\db\Schema;
use yii\db\Migration;

class m170520_114310_setting extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }
        
        $this->createTable('{{%setting}}', [
            'id' => Schema::TYPE_PK,
            'name' => Schema::TYPE_STRING . '(255) NULL',
            'key' => Schema::TYPE_STRING . '(255) NULL',
            'value' => Schema::TYPE_STRING . '(255) NULL',
            'date_create' => Schema::TYPE_DATETIME,
        ], $tableOptions);
        
        $this->batchInsert('{{%setting}}', ['name', 'key', 'value', 'date_create'], 
            [['email', 'email', 'dativStepan@gmail.com', time()]]);
    }

    public function safeDown()
    {
        $this->dropTable('{{%users}}');
    }
}
