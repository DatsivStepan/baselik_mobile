<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\helpers\Url;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\User;
use DateTimeZone;
use app\models\Video;
use app\models\PasswordResetRequestForm;
use app\models\ResetPasswordForm;

class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
            'eauth' => [
                // required to disable csrf validation on OpenID requests
                'class' => \nodge\eauth\openid\ControllerBehavior::className(),
                'only' => ['login'],
            ],
        ];
    }

    
    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        if(!Yii::$app->user->isGuest){
            return $this->render('index');            
        }else{
            return $this->redirect('site/login');
        }
    }

    /**
     * Login action.
     *
     * @return string
     */
//    public function actionLogin()
//    {
//        if (!Yii::$app->user->isGuest) {
//            return $this->goHome();
//        }
//
//        $model = new LoginForm();
//        if ($model->load(Yii::$app->request->post()) && $model->login()) {
//            return $this->goBack();
//        }
//        return $this->render('login', [
//            'model' => $model,
//        ]);
//    }
    public function actionLogin() {
        $serviceName = \Yii::$app->getRequest()->getQueryParam('service');
        if (isset($serviceName)) {
            /** @var $eauth \nodge\eauth\ServiceBase */
            $eauth = Yii::$app->get('eauth')->getIdentity($serviceName);
            $eauth->setRedirectUrl(Yii::$app->getUser()->getReturnUrl());
            $eauth->setCancelUrl(Yii::$app->getUrlManager()->createAbsoluteUrl('site/login'));

            try {
                if ($eauth->authenticate()) {
//                  var_dump($eauth->getIsAuthenticated(), $eauth->getAttributes()); exit;

                    $identity = User::findByEAuth($eauth);
                    $profile = $eauth->getAttributes();
                    //var_dump($profile);exit;
                    if(!User::find()->where(['username' => $profile['name'], 'email' => (isset($profile['email']))?$profile['email']:$profile['id']])->exists()){
                        $model = new User();
                        $model->username = (isset($profile['name']))?$profile['name']:$profile['id'];
                        $model->password = \Yii::$app->security->generateRandomString(8);
                        $model->password_hash = \Yii::$app->security->generatePasswordHash($model->password);
                        $model->auth_key = \Yii::$app->security->generateRandomString(8);
                        $model->username = $profile['name'];
                        $model->email = (isset($profile['email']))?$profile['email']:$profile['id'];
                        $model->signup_type = $eauth->getServiceName();
                        $model->social_id = (isset($profile['id']))?$profile['id']:'';
                        if ($model->save()) {
                          if (Yii::$app->getUser()->login($model)) {
                            $eauth->redirect();
                          }
                        }
                    }else{
                        $model = User::find()->where(['social_id' => (isset($profile['id']))?$profile['id']:$profile['id'],'username' => $profile['name'], 'email' => (isset($profile['email']))?$profile['email']:$profile['id']])->one();
                        if (Yii::$app->getUser()->login($model)) {
                          $eauth->redirect();
                        }
                    }
                    //Yii::$app->getUser()->login($identity);

                    // special redirect with closing popup window
//                    $eauth->redirect();
                }else {
                    // close popup window and redirect to cancelUrl
                    $eauth->cancel();
                }
            }
            catch (\nodge\eauth\ErrorException $e) {
                // save error to show it later
                Yii::$app->getSession()->setFlash('error', 'EAuthException: '.$e->getMessage());

                // close popup window and redirect to cancelUrl
//              $eauth->cancel();
                $eauth->redirect($eauth->getCancelUrl());
            }
        }
        
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        return $this->render('login', [
            'model' => $model,
        ]);
        
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
    
    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {

            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');
                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for email provided.');
            }
        }
        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }
    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }
        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'New password was saved.');
            return $this->goHome();
        }
        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }
    
    public function actionSignup(){
        if (!Yii::$app->user->isGuest) {
            return $this->redirect(Url::home());
        }
            $model = new User();

            $request = Yii::$app->request;
            $postedForm = $request->post('ContactForm');

            $model->scenario = 'signup';
            if ($model->load($request->post())){
                $model->password_hash = \Yii::$app->security->generatePasswordHash($model->password);
                $model->auth_key = 'key';
                if ($model->save()) {
                        if (Yii::$app->getUser()->login($model)) {
                                return $this->redirect(Url::home());
                        }
                }
            }

            return $this->render('signup', [
                'newModel' => $model,
            ]);
    }
    
    
//    ->join('LEFT JOIN',
//                    'users',
//                    'users.id = video.user_id')
//                ->join('LEFT JOIN',
//                    'users_contact',
//                    'users_contact.user_id = video.user_id')
//                ->join('LEFT JOIN',
//                    'category',
//                    'category.id = video.category_id')
    public function actionGetvideos()
    {
        if (isset($_POST['limit']) && isset($_POST['offset'])) {
            $modelVideos = Video::find()->where(['status' => 1])
                ->limit($_POST['limit'])
                ->offset($_POST['offset'])->all();
            $arrayVideos = [];
            foreach($modelVideos as $video){
                $arrayVideos[$video->id]['video_id'] = $video->id;
                $arrayVideos[$video->id]['video_src'] = $video->video_src;
                $arrayVideos[$video->id]['category_name'] = null;
                $arrayVideos[$video->id]['category_id'] = null;
                if($video->category){
                    $arrayVideos[$video->id]['category_id'] = $video->category->id;
                    $arrayVideos[$video->id]['category_name'] = $video->category->name;
                }
                
                $arrayVideos[$video->id]['user_id'] = null;
                $arrayVideos[$video->id]['user_name'] = null;
                if($video->user){
                    $arrayVideos[$video->id]['user_id'] = $video->user->id;
                    $arrayVideos[$video->id]['user_name'] = $video->user->lastname.' '.$video->user->name;
                    $arrayVideos[$video->id]['user_avatar'] = 'http://baselik.roketdev.pro/img/default_avatar.png';                        
                    if(($video->user->avatar != '') && ($video->user->avatar != null)){
                        $arrayVideos[$video->id]['user_avatar'] = $video->user->avatar;                        
                    }
                }
                
            }
//            if(($_POST['cityFilter'] != '') && ($_POST['cityFilter'] != 'empty') && ($_POST['cityFilter'] != 'undefined')){
//                $query->andWhere(['users_contact.address_city' => $_POST['cityFilter']]);
//            }
            
        } else {
            $arrayVideos = null;
        }

        echo json_encode($arrayVideos);
    }
    
    
    public function actionGetfavoritevideos()
    {
//        if (isset($_POST['limit']) && isset($_POST['offset'])) {
            $videos_id = $_POST['videos_id'];
            $modelVideos = Video::find()->where(['id' => $videos_id])->andWhere(['status' => 1])->all();
//                ->limit($_POST['limit'])
//                ->offset($_POST['offset'])->all();
            $arrayVideos = [];
            
            foreach($modelVideos as $video){
                $arrayVideos[$video->id]['video_id'] = $video->id;
                $arrayVideos[$video->id]['video_src'] = $video->video_src;
                $arrayVideos[$video->id]['category_name'] = null;
                $arrayVideos[$video->id]['category_id'] = null;
                if($video->category){
                    $arrayVideos[$video->id]['category_id'] = $video->category->id;
                    $arrayVideos[$video->id]['category_name'] = $video->category->name;
                }
                
                $arrayVideos[$video->id]['user_id'] = null;
                $arrayVideos[$video->id]['user_name'] = null;
                if($video->user){
                    $arrayVideos[$video->id]['user_id'] = $video->user->id;
                    $arrayVideos[$video->id]['user_name'] = $video->user->lastname.' '.$video->user->name;
                    $arrayVideos[$video->id]['user_avatar'] = 'http://baselik.roketdev.pro/img/default_avatar.png';                        
                    if(($video->user->avatar != '') && ($video->user->avatar != null)){
                        $arrayVideos[$video->id]['user_avatar'] = $video->user->avatar;                        
                    }
                }
                
            }
            
//        } else {
//            $arrayVideos = null;
//        }

        echo json_encode($arrayVideos);
    }
    
    public function actionGetvideo()
    {
        $video_id = $_POST['video_id'];
        $modelVideo = Video::find()->where(['id' => $video_id])->one();
        $arrayVideo = [];
        if($modelVideo){
            $arrayVideo['id'] = $modelVideo->id;
            $arrayVideo['video_src'] = $modelVideo->video_src;
            $arrayVideo['description'] = $modelVideo->description;
            $arrayVideo['user_id'] = $modelVideo->user->id;
            
            $arrayVideo['category_id'] = null;
            $arrayVideo['category_name'] = null;
            if(isset($modelVideo->category->id)){
                $arrayVideo['category_id'] = $modelVideo->category->id;
                $arrayVideo['category_name'] = $modelVideo->category->name;                
            }
            
            
            $arrayVideo['city'] = null;
            $arrayVideo['telefon'] = null;
            $arrayVideo['viber'] = null;
            $arrayVideo['skype'] = null;
            $arrayVideo['whatsapp'] = null;
            $arrayVideo['time_work_start'] = null;
            $arrayVideo['time_work_end'] = null;
            if(isset($modelVideo->user->usercontact)){
                $arrayVideo['city'] = $modelVideo->user->usercontact->address_city;
                $arrayVideo['telefon'] = $modelVideo->user->usercontact->telefon;
                $arrayVideo['viber'] = $modelVideo->user->usercontact->viber;
                $arrayVideo['skype'] = $modelVideo->user->usercontact->skype;
                $arrayVideo['whatsapp'] = $modelVideo->user->usercontact->whatsapp;
                $arrayVideo['time_work_start'] = $modelVideo->user->usercontact->time_work_start;
                $arrayVideo['time_work_end'] = $modelVideo->user->usercontact->time_work_end;                
            }
            
            $arrayVideo['user_name'] = $modelVideo->user->lastname.' '.$modelVideo->user->name;
            $arrayVideo['user_avatar'] = 'http://baselik.dev/img/default_avatar.png';
            if(($modelVideo->user->avatar != '') && ($modelVideo->user->avatar != null)){
                $arrayVideo['user_avatar'] = $modelVideo->user->avatar;                        
            }    
            
            $arrayVideo['others_videos'] = [];
            if(isset($modelVideo->category->id)){
                $modelOtherVideos = Video::find()->where(['category_id' =>$modelVideo->category->id])->andWhere(['not', ['id' => $modelVideo->id]])->limit(4)->all();
                foreach($modelOtherVideos as $videos){
                    $arrayVideo['others_videos'][] = [
                            'id' => $videos->id,
                            'video_src' => $videos->video_src,
                            'user_name' => $videos->user->name.' '.$videos->user->lastname.' '.$videos->user->surname,
                            'category_name' => $videos->category->name
                        ];
                }
            }
            
        }else{
            $arrayVideo = null;            
        }
        
        echo json_encode($arrayVideo);
    }
    
    public function actionGetuserdata()
    {
        $user_id = $_POST['user_id'];
        $user_array = [];
        $modelUser = User::find()->where(['id' => $user_id])->one();
        if($modelUser){
            $user_array['name'] = $modelUser->name;
            $user_array['surname'] = $modelUser->surname;
            if($modelUser->gender == 0){
                $user_array['gender'] = 'мужской';
            }else{
                $user_array['gender'] = 'женский';                
            }
            $user_array['birthday'] = $modelUser->birthday;
            
            $time = $modelUser->date_create;
            $datetime1 = date_create($time);
            $datetime2 = date_create('now',new DateTimeZone('Europe/Moscow'));
            $interval = date_diff($datetime1, $datetime2);
            $date_s = '';
            if($interval->y == 0){
                if($interval->m == 0){
                    $date_s = $interval->d.' дней';
                }else{
                    $date_s = $interval->m.' месяцев '.$interval->d.' дней';
                }
            }else{
                $date_s = $interval->y.' лет '.$interval->m.' месяцев '.$interval->d.' дней';
            }
            $user_array['in_site'] = $date_s;
            $user_array['username'] = $modelUser->username;
            
            $user_array['user_avatar'] = 'http://baselik.roketdev.pro/img/default_avatar.png';
            if(($modelUser->avatar != '') && ($modelUser->avatar != null)){
                $user_array['user_avatar'] = $modelUser->avatar;                        
            }
            
            $user_array['email'] = $modelUser->email;
            $user_array['spec_count'] = \app\models\Usercategory::find()->where(['user_id' => $user_id])->count();;
        }
        
        echo json_encode($user_array);
    }
    
    public function actionGetusercontact(){
        $user_id = $_POST['user_id'];
        $user_array = [];
        $modelUser = User::find()->where(['id' => $user_id])->one();
        $modelUserContact = \app\models\Usercontact::find()->where(['user_id' => $user_id])->one();
        if($modelUserContact){
            $user_array['address'] = $modelUserContact->address_city;
            $user_array['telefon'] = $modelUserContact->telefon;
            $user_array['email'] = $modelUser->email;
            $user_array['work_time'] = $modelUserContact->time_work_start.' - '.$modelUserContact->time_work_end;
            $user_array['whatsapp'] = $modelUserContact->whatsapp;
            $user_array['viber'] = $modelUserContact->viber;
            $user_array['skype'] = $modelUserContact->skype;
                       
        }
        echo json_encode($user_array);
    }
    
    public function actionSaveprofiledata(){
        $result = [];
        if($_POST){
            $modelUser = User::find()->where(['id' => \Yii::$app->user->id])->one();
            $modelUser->scenario = 'update_profile';
            $modelUser->name = $_POST['name'];
            $modelUser->surname = $_POST['surname'];
            $modelUser->gender = $_POST['gender'];
            $modelUser->birthday = $_POST['birthday'];
            $modelUser->email = $_POST['email'];
            $modelUser->avatar = $_POST['avatar'];
            if($modelUser->save()){
                $result['status'] = 'save';                
            }else{
                $result['status'] = 'error 1';            
            }
        }else{
            $result['status'] = 'error 2';            
        }
        echo json_encode($result);
    }
    
    
    public function actionSaveavatar(){
        $ds          = DIRECTORY_SEPARATOR;  //1
        $storeFolder = \Yii::getAlias('@webroot').'/images/users_images';   //2
        // mkdir($storeFolder);
        if (!empty($_FILES)) {
            if(!is_dir($storeFolder.$ds.Yii::$app->user->id)){
              mkdir($storeFolder.$ds.Yii::$app->user->id, 0777, true);
            }
            
            $tempFile = $_FILES['file']['tmp_name'];          //3
            $targetPath =  $storeFolder . $ds.Yii::$app->user->id.$ds;  //4
            $for_name = time();
            $randNumber = rand(10,99).rand(10,99);
            $targetFile =  $targetPath.$for_name.'_'.$randNumber.'.png';  //5

            move_uploaded_file($tempFile,$targetFile); //6
            return 'images/users_images/'.\Yii::$app->user->id.'/'.$for_name.'_'.$randNumber.'.png'; //5
        }
    }
    
    
    
}
