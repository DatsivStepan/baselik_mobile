$(document).ready(function(){
    var paginations = {
        limit: 4,
        offset: 0
    };
    var paginations_f = {
        limit: 4,
        offset: 0
    };

    $(document).on('click', '.profile_show .edit_profile_button', function(){
        $(this).hide();
        $('.profile_show .user_avatar').hide();
        $('.profile_show .cancel_profile_button').show();

        $('.profile_show .user_information_block').hide();
        $('.profile_show .username_b').hide();
        $('.profile_show .user_change_information_block').show();
        $('.profile_show .change_avatar_button').show();
    });

    $(document).on('click', '.profile_show .cancel_profile_button', function(){
        $(this).hide();
        $('.profile_show .user_avatar').show();
        $('.profile_show .profile').hide();
        $('.profile_show .edit_profile_button').show();

        $('.profile_show .user_information_block').show();
        $('.profile_show .username_b').show();
        $('.profile_show .user_change_information_block').hide();
        $('.profile_show .change_avatar_button').hide();
    });


    function getVideos(){
//        var category_id = $('.GetCategory_id').val();
//        var without_video_id = $('.without_video_id').val();
        $.ajax({
            type: 'POST',
            url: '../../../../site/getvideos',
            data: {
//                category_id:category_id,
//                without_video_id:without_video_id,
                limit: paginations.limit,
                offset: paginations.offset
            },
            dataType: "json",
            success: function(response){
                console.log(response);
                if(response.length < 4){
                    $(".show_more_video_btn").hide();
                }
//                //$('.videoBoxContent').html('');
                for(var key in response){
                    $('.videoBoxContent').append("<div class='col-sm-6 col-xs-12 las_video_33'>"+
                        "<div class='video_item prem' >"+
                            "<a class='video_link' data-video_id='"+response[key].video_id+"'>"+
                                "<img class='video_img' src='//img.youtube.com/vi/"+response[key].video_src+"/0.jpg'  alt='img'>"+
                            '</a>'+
                            '<div class="clas_padd_ic2" style="padding: 20px 20px;">'+
                                '<div class="col-xs-3 padding_0">'+
                                    '<img class="img_vid_ff" src="'+response[key].user_avatar+'" style="width:100%;border-radius:50%;">'+
                                '</div>'+
                                '<div class="col-xs-7 headerInformationBlock" style="padding-left:0px;padding-top:4px;">'+
                                    '<p class="vedeo_art" style="margin-bottom:0px;font-size:17px;color:#788277;font-weight:700;">'+response[key].category_name+'</p>'+
                                    '<p class="vedeo_text" style="color:#788277;">'+response[key].user_name+'</p>'+
                                '</div>'+
                                '<div class="col-xs-2 favoriteIconBlock favoti_left favotiteId'+response[key].video_id+'" data-status="no_active" data-video_id="'+response[key].video_id+'" style="padding-top: 16px;padding-left: 10px;">'+
                                    '<img class="img_zir" src="/images/favorite.png">'+
                                '</div>'+
                                '<div style="clear:both;">'+
                                '</div>'+
                            '</div>'+
                        '</div>'+
                    '</div>');
                    checkFavorite(response[key].video_id)
                }
            }
        });
    }
    getVideos();


    function checkFavorite(video_id){
        if($.cookie('favorite_videos')){
            if($.cookie('favorite_videos') != 'null'){
                var video_list = jQuery.parseJSON($.cookie('favorite_videos'));
                if($.inArray(video_id,video_list) >= 0){
                    $('.favotiteId'+video_id).data('status', 'active')
                    $('.favotiteId'+video_id).find('img').attr('src', '/images/favorite_active.png')
                }else{
                    $('.favotiteId'+video_id).data('status', 'no_active')
                    $('.favotiteId'+video_id).find('img').attr('src', '/images/favorite.png')
                }
            }else{}
        }else{}
    }

    $(document).on('click', '.favoriteIconBlock', function(){
        var status = $(this).data('status');
        var video_id = $(this).data('video_id');
        if(status == 'no_active'){
            if($.cookie('favorite_videos')){
                if($.cookie('favorite_videos') != 'null'){
                    var video_list = jQuery.parseJSON($.cookie('favorite_videos'));
                    video_list.push(video_id)
                    $.cookie('favorite_videos', JSON.stringify(video_list));
                }else{
                    var video_list = [];
                    video_list.push(video_id)
                    $.cookie('favorite_videos', JSON.stringify(video_list));
                }
            }else{
                var video_list = [];
                video_list.push(video_id)
                $.cookie('favorite_videos', JSON.stringify(video_list));
            }
            $('.favotiteId'+video_id).data('status', 'active')
            $('.favotiteId'+video_id).find('img').attr('src', '/images/favorite_active.png')
        }else{
            if($.cookie('favorite_videos')){
                if($.cookie('favorite_videos') != 'null'){
                    var video_list = jQuery.parseJSON($.cookie('favorite_videos'));
                    video_list = jQuery.grep(video_list, function(value) {
                        return value != video_id;
                    });
                    $.cookie('favorite_videos', JSON.stringify(video_list));
                }
            }
            $('.favotiteId'+video_id).data('status', 'no_active')
            $('.favotiteId'+video_id).find('img').attr('src', '/images/favorite.png')

        }

    })


    $(".show_more_video_btn").click(function(e){
        paginations.offset += paginations.limit;
        getVideos();
        e.preventDefault();
    });

    function getFavoriteVideo(){

        if($.cookie('favorite_videos')){
            if($.cookie('favorite_videos') != 'null'){
                console.log($.cookie('favorite_videos'))
                var video_list = jQuery.parseJSON($.cookie('favorite_videos'));
                if(video_list.length != 0){


                            $.ajax({
                                type: 'POST',
                                url: '../../../../site/getfavoritevideos',
                                data: {
                                    videos_id:video_list,
//                                    limit: paginations_f.limit,
//                                    offset: paginations_f.offset
                                },
                                dataType: "json",
                                success: function(response){
//                                    if(response.length < 4){
//                                        $(".show_more_favorite_video_btn").hide();                    
//                                    }
                                    $('.favoriteVideoBlock').html('');
                                    for(var key in response){
                                        $('.favoriteVideoBlock').append("<div class='col-sm-6 col-xs-12'>"+
                                            "<div class='video_item prem' >"+
                                                "<a class='video_link' data-video_id='"+response[key].video_id+"'>"+
                                                    "<img class='video_img' src='//img.youtube.com/vi/"+response[key].video_src+"/0.jpg'  alt='img'>"+
                                                '</a>'+
                                                '<div style="padding:15px 15px;">'+
                                                    '<div class="col-xs-3 padding_0">'+
                                                        '<img class="img_vid_ff" src="'+response[key].user_avatar+'" style="width:100%;border-radius:50%;">'+
                                                    '</div>'+
                                                    '<div class="col-xs-7 headerInformationBlock" style="padding-left:0px;padding-top:4px;">'+
                                                        '<p class="vedeo_art" style="margin-bottom:0px;font-size:18px;color:#788277;font-weight:700;">'+response[key].category_name+'</p>'+
                                                        '<p class="vedeo_text" style="color:#788277;">'+response[key].user_name+'</p>'+
                                                    '</div>'+
                                                    '<div class="col-xs-2 favoriteIconBlock favoti_left favotiteId'+response[key].video_id+'" data-status="no_active" data-video_id="'+response[key].video_id+'" style="padding-top: 16px;padding-left: 10px;">'+
                                                        '<img class="img_zir" src="/images/favorite.png" style="width:100%;">'+
                                                    '</div>'+
                                                    '<div style="clear:both;">'+
                                                    '</div>'+
                                                '</div>'+
                                            '</div>'+
                                        '<div style="clear:both;">'+
                                        '</div>');
                                        checkFavorite(response[key].video_id)
                                    }
                                }
                            });
                }else{
                    $('.favoriteVideoBlock').html('<p style="text-align:center;">пока в избранном ничего нет</p>');
                }
            }else{
                $('.favoriteVideoBlock').html('<p style="text-align:center;">пока в избранном ничего нет</p>');
            }
        }else{
                $('.favoriteVideoBlock').html('<p style="text-align:center;">пока в избранном ничего нет</p>');
        }

    }

    $(document).on('click', '.video_link', function(){
        var video_id = $(this).data('video_id');
        $.ajax({
            type: 'POST',
            url: '../../../../site/getvideo',
            data: {
                video_id: video_id
            },
            dataType: "json",
            success: function(response){
                $('.video_show').html('');
                $('.video_show').show();
                if(response){
                    $('.video_show').append(''+
                            '<div class="col-sm-7 col-xs-12" style="padding:0px;">'+
                                '<div class="arr_dd">'+
                                    '<div class="video_show_arrow_back"></div>'+
                                '</div>'+
                                '<iframe class="clas_img_rr" src="https://www.youtube.com/embed/'+response['video_src']+'?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>'+
                                        '<div style="padding:10px;">'+
                                            '<div style="box-shadow: 0 0 18px rgba(0,0,0,0.1);padding: 5px 5px;">'+
                                                '<div style="padding:10px;">'+
                                                    '<div class="col-xs-2 col-sm-3" style="padding:0px;">'+
                                                        '<img class="img_vid_ff" src="'+response['user_avatar']+'">'+
                                                    '</div>'+
                                                    '<div class="col-xs-9 col-sm-9" style="padding-left:5px;">'+
                                                        '<p class="vedeo_art22" style="margin-bottom:0px;font-size:17px;color:#788277;font-weight:700;">'+response['category_name']+'</p>'+
                                                        '<p class="vedeo_text22" style="color:#788277;">'+response['user_name']+'</p>'+
                                                    '</div>'+
                                                    '<div style="clear:both;"></div>'+
                                                '</div>'+
                                                '<div class="col-xs-12 videoInformationBlock">'+
                                                    '<p class="eye" style="color:#879285;">'+
                                                        '5 265 / 125 просмотров'+
                                                    '</p>'+
                                                    '<p class="calendar" style="color:#879285;">'+
                                                        'Размещено 12.02.2017'+
                                                    '</p>'+
                                                    '<p class="star" style="color:#879285;">'+
                                                        'Добавили в избранное 15'+
                                                    '</p>'+
                                                '</div>'+
                                                '<div class="col-xs-12 desc_cate_ff">'+
                                                    response['description']+
                                                '</div>'+
                                                '<div style="clear:both;"></div>'+
                                            '</div>'+
                                        '</div>'+
                            '</div>'+
                            '<div class="col-sm-5 col-xs-12" style="padding:10px;">'+
                                    '<div style="box-shadow: 0 0 18px rgba(0,0,0,0.1);padding: 5px 5px;">'+
                                        '<div style="border-bottom:#ebeeee;padding:10px;">'+
                                            '<h3 class="h3_des">Другие исполнители</h3>'+
                                            '<div class="otherVideoBlock">'+
                                            '</div>'+
                                        '</div>'+
                                    '</div>'+
                            '</div>'+
                            '');
                    if(response['others_videos']){
                        for(var key in response['others_videos']){
                            var o_video = response['others_videos'][key]
                            $('.otherVideoBlock').append(''+
                                    '<div style="margin-top:10px;">'+
                                        '<div class="col-xs-4" style="padding-left:0px;">'+
                                            "<img src='//img.youtube.com/vi/"+o_video.video_src+"/0.jpg'  alt='img' style='width:100%;'>"+
                                        '</div>'+
                                        '<div class="col-xs-8">'+
                                            '<p class="vedeo_art22 vedeo_art2233" style="margin-bottom:0px;font-size:17px;color:#788277;font-weight:700;">'+o_video['category_name']+'</p>'+
                                            '<p class="vedeo_text22 vedeo_text2233" style="color:#788277;">'+o_video['user_name']+'</p>'+
                                        '</div>'+
                                    '</div>'+
                                    '<div style="clear:both;"></div>'+
                                    '');
                        }
                    }
                }
                console.log(response)
            }
        })
    });


    $(document).on('click', '.video_show_arrow_back', function(){
        $('.video_show').hide();
    });

    $(document).on('click', '.videoFilterButton', function(){
        hideShowBlocks();
        $('.video_filter').show();
    })
    $(document).on('click', '.video_filter_arrow_back', function(){
        $('.video_filter').hide();
    })

    function getUserProfileData(user_id){
            $.ajax({
                type: 'POST',
                url: '../../../../site/getuserdata',
                data: {
                    user_id: user_id
                },
                dataType: "json",
                success: function(response){
                    $('.profile_show').find('.user_avatar').attr('src', response['user_avatar'])
                    $('.profile_show .user_change_information_block').find('.user_avatar_change').attr('src', response['user_avatar'])
                    $('.profile_show').find('.username_b').text(response['surname']+' '+response['name'])
                    var user_block = $('.profile_show .user_information_block');
                    for(var key in response){
                        user_block.find('.'+key).text(response[key]);
                    }
                    if(response['gender'] == 'женский'){
                       $(".profile_show .user_change_information_block #woman").attr('checked', 'checked');
                    }else{
                       $(".profile_show .user_change_information_block #man").attr('checked', 'checked');
                    }
                    $(".profile_show .user_change_information_block").find('input[name=birthday]').val(response['birthday']);
                    $(".profile_show .user_change_information_block").find('input[name=email]').val(response['email']);
                    $(".profile_show .user_change_information_block").find('input[name=name]').val(response['name']);
                    $(".profile_show .user_change_information_block").find('input[name=surname]').val(response['surname']);
                    $(".profile_show").find('#avatar_src').val(response['user_avatar']);
                }
            });
    }

    function getUserContact(user_id){
            $.ajax({
                type: 'POST',
                url: '../../../../site/getusercontact',
                data: {
                    user_id: user_id
                },
                dataType: "json",
                success: function(response){
                    var user_block = $('.contact_block .user_information_block');
                    for(var key in response){
                        user_block.find('.'+key).text(response[key]);
                    }
                }
            });
    }

    $(document).on('click', '.left_menu_sidebar .profile_show_button, .home_menu_user', function(){
        hideShowBlocks();
        $('.container-main.st-effect-1').removeClass('st-menu-open')
        $('body').css('overflow','auto')
        $('.profile_show').show();

        getUserProfileData($(this).data('user_id'));
    });

    $(document).on('click', '.favoriteShowButton', function(){
        hideShowBlocks();
        $('.container-main.st-effect-1').removeClass('st-menu-open')
        $('body').css('overflow','auto')
        $('.favorite_block').show();
        getFavoriteVideo();
    });

    $(document).on('click', '.contactShowButton', function(){
        hideShowBlocks();
        $('.container-main.st-effect-1').removeClass('st-menu-open')
        $('body').css('overflow','auto')
        $('.contact_block').show();
        getUserContact($(this).data('user_id'));
    });

    $(document).on('click', '.contact_arrow_back', function(){
        $('.contact_block').hide();
    })
    $(document).on('click', '.favotite_arrow_back', function(){
        $('.favorite_block').hide();
    })

    $(document).on('click', '.profile_show_arrow_back', function(){
        $('.profile_show').hide();
    })

    if($.cookie('filter_category')){
        if($.cookie('filter_category') != 'null'){
        }
    }


    function hideShowBlocks(){
        $('.favorite_block').hide()
        $('.profile_show').hide()
        $('.contact_block').hide()
        $('.video_filter').hide()
        $('.video_show').hide();
    }

    function calculateShowBlockWidth(){
        $('.favorite_block').width($('.favorite_block').parent().width());
        $('.profile_show').width($('.profile_show').parent().width());
        $('.contact_block').width($('.contact_block').parent().width());
        $('.video_filter').width($('.video_filter').parent().width());
        $('.video_show').width($('.video_show').parent().width());
    }
    calculateShowBlockWidth();
    $( window ).resize(function() {
        calculateShowBlockWidth();
    })



    if($('.change_avatar_button').length){
        var previewNode = document.querySelector("#template");
        previewNode.id = "";
        var previewTemplate = previewNode.parentNode.innerHTML;
        previewNode.parentNode.removeChild(previewNode);

        var myDropzone = new Dropzone(document.body, {
            url: "../../../site/saveavatar",
            previewTemplate: previewTemplate,
            previewsContainer: "#previews",
            clickable: ".change_avatar_button",
            uploadMultiple:false,
            maxFiles:1,
        });

        myDropzone.on("maxfilesexceeded", function(file) {
            myDropzone.removeAllFiles();
            myDropzone.addFile(file);
        });

        myDropzone.on("complete", function(response) {
            if (response.status == 'success') {
                $('.profile_show .user_avatar_change').attr('src', '/'+response.xhr.response);
                $('.profile_show #avatar_src').val('http://baselik-mobile.roketdev.pro/'+response.xhr.response);
            }
        });

        myDropzone.on("removedfile", function(response) {
            if(response.xhr != null){
               //deleteFile(response.xhr.response);
            }
        });
    }

    $(document).on('click', '.saveProfileData', function(){
            var info_block = $(".profile_show .user_change_information_block");
            var gender = info_block.find('input[name=gender]:checked').val();
            var birthday = info_block.find('input[name=birthday]').val();
            var email = info_block.find('input[name=email]').val();
            var name = info_block.find('input[name=name]').val();
            var surname = info_block.find('input[name=surname]').val();
            var avatar = info_block.find('#avatar_src').val();
            var data = {
                    gender: gender, birthday: birthday, email: email, name: name,
                    surname: surname, avatar: avatar
                };
            $.ajax({
                type: 'POST',
                url: '../../../../site/saveprofiledata',
                data: data,
                dataType: "json",
                success: function(response){
                    if(response.status == 'save'){
                        $('.profile_show .cancel_profile_button').hide();
                        $('.profile_show .user_avatar').show();
                        $('.profile_show .profile').hide();
                        $('.profile_show .edit_profile_button').show();

                        $('.profile_show .user_information_block').show();
                        $('.profile_show .username_b').show();
                        $('.profile_show .user_change_information_block').hide();
                        $('.profile_show .change_avatar_button').hide();
                        var user_block = $('.profile_show .user_information_block');
                        for(var key in data){
                            user_block.find('.'+key).text(data[key]);
                            if(key == 'gender'){
                                if(data[key] == '1'){
                                    user_block.find('.'+key).text('Женский');
                                }else{
                                    user_block.find('.'+key).text('Мужской');
                                }
                            }
                        }
                        $('.left_menu_avatar').attr('src', avatar)
                        $('.profile_show').find('.username_b').text(surname+' '+name);
                        $('.profile_show').find('.user_avatar').attr('src',avatar );
                    }
                }
            })
    })

})