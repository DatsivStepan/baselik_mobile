<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ideas".
 *
 * @property integer $id
 * @property integer $project_id
 * @property string $ideas_description
 * @property integer $stormer_id
 */
class Productcategory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product_category';
    }

    public function scenarios()
    {
        return [
            'add' => ['product_id', 'category_id'],
        ];
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            
        ];
    }
}
