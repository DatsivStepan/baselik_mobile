<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
//        "https://code.jquery.com/mobile/1.4.5/jquery.mobile-1.4.5.min.css",
        'css/bootstrap-datetimepicker.css',
        'css/fonts_h.css',
        'css/demo.css',
        'css/icons.css',
        'css/normalize.css',
        'css/component.css',
        'css/site.css',
    ];
    public $js = [
//        "https://code.jquery.com/mobile/1.4.5/jquery.mobile-1.4.5.min.js",
        'js/moment.js',
        'js/plugins/dropzone.js',
        'js/bootstrap-datetimepicker.js',
        'js/jquery-cookie.js',
        'js/modernizr.custom.js',
        'js/classie.js',
        'js/sidebarEffects.js',
        'js/video_list.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
