<?php

return [
    'adminEmail' => 'admin@example.com',
    'supportEmail' => 'datsivStepan@gmail.com',
    'user.passwordResetTokenExpire' => '259200',
];
