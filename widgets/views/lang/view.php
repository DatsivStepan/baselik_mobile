<?php
    use yii\helpers\Html;
    use yii\helpers\Url;
?>

<li class="dropdown">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true"><img src="<?= Url::home().'flags/'.$current->icon;?>"> <?= $current->name;?> <span class="caret"></span></a>
    <ul class="dropdown-menu">
        <?php foreach ($langs as $lang):?>
            <li>
                <?= Html::a('<img src="'.Url::home().'flags/'.$lang->icon.'"> '.$lang->name, Url::home().$lang->url.Yii::$app->getRequest()->getLangUrl()) ?>
            </li>
        <?php endforeach;?>
    </ul>
</li>